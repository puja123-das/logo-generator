<?php require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

$imagePath = './data/images/image.jpg'; 

$logoText = empty($_GET['t'])? 'Test Funnel': $_GET['t'];
$logoColor = empty($_GET['c'])? '#000000': $_GET['c'];
$logoFont = empty($_GET['f'])? 'alexbrush': $_GET['f'];

$builder = new CodeClouds\LogoGenerator\Transparent(
	$logoText, 100, $logoColor, $logoFont
);

$builder->show();

// $builder->save('output.png');

