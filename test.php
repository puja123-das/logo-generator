<?php
require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$obj= new CodeClouds\LogoGenerator\LogoGenerator('data/images/image.jpg', "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", 
	80, 'courier-new-bold-italic');

// // x stands for horizontal distance, Y stands for vertical distance
$obj->setPosition(240, 520);
$obj->setTextColor(230, 240, 230);
$obj->setQuality(95);
$obj->show();
 