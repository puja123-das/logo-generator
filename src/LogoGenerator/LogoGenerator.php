<?php

namespace CodeClouds\LogoGenerator;

use Exception;

class LogoGenerator
{
    const DS = DIRECTORY_SEPARATOR;
    protected $img, $textColor, $bgImagePath, $text, $fontFile,
    $fontSize, $format, $jpegQuality, $lines;
    protected $position, $startPosition = array();

    const ERROR_FORMAT = 'Not Supported File';

    public function __construct($bgImagePath, $text, $fontSize, $fontStyle = null)
    {
        $this->bgImagePath = $bgImagePath;
        $this->text        = $text;
        $fontStyle         = strtolower($fontStyle);
        $this->fontFile    = dirname(dirname(__DIR__))
        . self::DS . 'data' . self::DS
        . 'fonts' . self::DS . $fontStyle . '.ttf';
        if (!file_exists($this->fontFile)) {
            throw new Exception('font style not found');

        }
        $this->fontSize = $fontSize;
        $this->setFormat();
        $this->setQuality();
        $this->createImage();
        $this->setTextColor();
        $this->setPosition();
        $this->getTextDimension();
    }

    public function setFormat()
    {

        $pathInfo     = pathinfo($this->bgImagePath);
        $extension    = $pathInfo['extension'];
        $this->format = strtoupper($extension);
        if ($this->format == 'JPEG' || $this->format == 'JPG') {
            $this->format = 'JPEG';
        } elseif ($this->format == 'PNG') {
            $this->format = 'PNG';
        } elseif ($this->format == 'GIF') {
            $this->format = 'GIF';
        } elseif ($this->format == 'WBMP') {
            $this->format = 'WBMP';
        } else {
            throw new Exception(self::ERROR_FORMAT);
        }
    }

    public function setQuality($value = 85)
    {

        $this->jpegQuality = $value;
    }

    public function createImage()
    {

        if ($this->format == 'JPEG') {
            $this->img = imagecreatefromjpeg($this->bgImagePath);
        } elseif ($this->format == 'PNG') {
            $this->img = imagecreatefrompng($this->bgImagePath);
        } elseif ($this->format == 'GIF') {
            $this->img = imagecreatefromgif($this->bgImagePath);
        } elseif ($this->format == 'WBMP') {
            $this->img = imagecreatefromwbmp($this->bgImagePath);
        } else {
            throw new Exception(self::ERROR_FORMAT);
        }
        return $this->img;
    }

    public function setTextColor($r = 0, $g = 0, $b = 0)
    {

        $this->textColor = imagecolorallocate($this->img, $r, $g, $b);
    }

    public function getTextDimension()
    {

        if ($this->fontSize <= 40) {
            $this->lines = explode('|', wordwrap($this->text, 70, '|'));
        } else {
            $this->lines = explode('|', wordwrap($this->text, 20, '|'));
        }
    }

    public function setPosition($x = 200, $y = 513)
    {
        $this->position['x'] = $x;
        $this->position['y'] = $y;
    }

    public function show()
    {
        header('content-type: image/' . $this->format);
        foreach ($this->lines as $line) {
            imagettftext($this->img, $this->fontSize, 0, $this->position['x'], $this->position['y'], $this->textColor, $this->fontFile, $line
            );
            $this->position['y'] += 100;
        }
        switch ($this->format) {
            case 'JPEG':
                imagejpeg($this->img, null, $this->jpegQuality);
                break;
            case 'PNG':
                imagepng($this->img);
                break;
            case 'GIF':
                imagegif($this->img);
                break;
            case 'WBMP':
                imagewbmp($this->img);
                break;
            default:
                imagepng($this->img);
                break;
        }
    }

}
