<?php

namespace CodeClouds\LogoGenerator;

use Exception;

class Transparent
{

    const INVALID_COLOR_FORMAT = 'Invalid Color Format';
    const DS                   = DIRECTORY_SEPARATOR;

    private $image, $text, $fontSize, $fontColor, $fontFile;
    private $imageWidth, $imageHeight, $cursorX, $cursorY;

    public function __construct($text, $fontSize, $fontColor, $fontType)
    {

        $this->text     = empty(trim($text)) ? 'LOGO' : trim($text);
        $this->fontSize = empty($fontSize) ? 16 : (int) $fontSize;

        $fontDir = sprintf(
            '%s%sstorage%sfonts%s', dirname(dirname(__DIR__)),
            self::DS, self::DS, self::DS
        );

        $this->fontFile = $fontDir . strtolower($fontType) . '.ttf';

        if (!file_exists($this->fontFile)) {
            throw new Exception(sprintf('Font: %s not found!', $fontType));
        }

        $this->prepareImageSizeAndCursor();

        $this->prepareFreshImageInstance();

        list($R, $G, $B) = sscanf($fontColor, "#%02x%02x%02x");
        if((int)$R < 10 && (int)$G < 10 && (int)$B < 10){
            $R = $G = $B = 10;
        }

        $this->color = imagecolorallocate($this->image, $R, $G, $B);

    }

    public function save($outputFile)
    {

        imagettftext(
            $this->image, $this->fontSize, 0, $this->cursorX, $this->cursorY,
            $this->color, $this->fontFile, $this->text
        );

        imagepng($this->image, $outputFile);
    }

    public function show()
    {

        imagefill(
            $this->image , 0, 0, imagecolorallocate($this->image, 255, 255, 255)
        );

        imagettftext(
            $this->image, $this->fontSize, 0, $this->cursorX, $this->cursorY,
            $this->color, $this->fontFile, $this->text
        );

        header('Content-Type: image/png');
        imagepng($this->image);
    }

    private function hexCodeToRGB($hexCode){
        
    }

    private function prepareFreshImageInstance()
    {
        $this->image = imagecreatetruecolor($this->imageWidth, $this->imageHeight);
        $black       = imagecolorallocate($this->image, 0, 0, 0);
        imagecolortransparent($this->image, $black);
    }

    private function prepareImageSizeAndCursor()
    {
        $p = imagettfbbox($this->fontSize, 0, $this->fontFile, $this->text);

        $this->imageWidth  = abs(max($p[2], $p[4]) - min($p[0], $p[6]));
        $this->imageHeight = abs(max($p[1], $p[3]) - min($p[5], $p[7]));

        $this->cursorX = (-1 * min($p[0], $p[6])) + $this->fontSize / 2;
        $this->cursorY = (-1 * min($p[5], $p[7])) + $this->fontSize / 2;

        $this->imageWidth  = $this->imageWidth + $this->fontSize;
        $this->imageHeight = $this->imageHeight + $this->fontSize;
    }

    public function __destruct()
    {
        imagedestroy($this->image);
    }

}
