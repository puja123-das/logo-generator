<?php

namespace CodeClouds\LogoGenerator;

use Exception;

class Builder
{

    const EXCEPTION_INVALID_IMAGE = 'Background image: Invalid image file';
    const DS                      = DIRECTORY_SEPARATOR;
    const INVALID_COLOR_FORMAT    = 'Invalid Color Format';

    private $imageExtension, $text, $extension, $backgroundImage, $fontFile;
    private $fontColor, $fontFamily, $height, $width, $cursorX, $cursorY;
    private $fontSize, $colorR = 0, $colorG = 0, $colorB = 0;

    public function __construct($backgroundImagePath, $text, $fontType)
    {
        if (!file_exists($backgroundImagePath)) {
            throw new Exception(
                sprintf(
                    'Background image: %s not found!', $backgroundImagePath
                )
            );
        }

        $pathinfo = pathinfo($backgroundImagePath);
        if (empty($pathinfo['extension'])) {
            throw new Exception(self::EXCEPTION_INVALID_IMAGE);
        }
        $this->extension = $this->getExtension($backgroundImagePath);

        $imageCreatorFunction = 'imagecreatefrom' . $this->extension;

        $this->backgroundImage = @$imageCreatorFunction($backgroundImagePath);

        if (!$this->backgroundImage) {
            throw new Exception(self::EXCEPTION_INVALID_IMAGE);
        }

        $sizeInformation = getimagesize($backgroundImagePath);
        $this->width     = $sizeInformation[0];
        $this->height    = $sizeInformation[0];
        $this->text      = $text;

        $fontDir = sprintf(
            '%s%sstorage%sfonts%s', dirname(dirname(__DIR__)), self::DS, self::DS, self::DS
        );

        $this->fontFile = $fontDir . strtolower($fontType) . '.ttf';

        if (!file_exists($this->fontFile)) {
            throw new Exception(sprintf('Font: %s not found!', $fontType));
        }
        $this->prepareCursorForCenterAlign();
    }

    private function getExtension($filePath)
    {
        $pathinfo = pathinfo($filePath);
        if (empty($pathinfo['extension'])) {
            throw new Exception(self::EXCEPTION_INVALID_IMAGE);
        }
        return (strtolower($pathinfo['extension']) === 'jpg' ?
            'jpeg' : strtolower($pathinfo['extension'])
        );
    }

    public function getTextColor()
    {
        $r    = dechex($this->colorR);
        $g    = dechex($this->colorG);
        $b    = dechex($this->colorB);
        $hexR = strlen($r) < 2 ? '0' . $r : $r;
        $hexG = strlen($g) < 2 ? '0' . $g : $g;
        $hexB = strlen($b) < 2 ? '0' . $b : $b;
        return '#' . $hexR . $hexG . $hexB;
    }

    public function setTextColor($hexColor)
    {
        if (!empty($hexColor)) {
            if (substr($hexColor, 0, 1) == '#') {
                $hexColorWithoutHash = substr($hexColor, 1);
                $r                   = substr($hexColorWithoutHash, 0, 2);
                $g                   = substr($hexColorWithoutHash, 2, 2);
                $b                   = substr($hexColorWithoutHash, 4, 2);

                $red   = hexdec($r);
                $green = hexdec($g);
                $blue  = hexdec($b);

                $RGB['R']     = $red;
                $RGB['G']     = $green;
                $RGB['B']     = $blue;
                $this->colorR = $RGB['R'];
                $this->colorG = $RGB['G'];
                $this->colorB = $RGB['B'];
            } else {
                throw new Exception(self::INVALID_COLOR_FORMAT);
            }
        }
    }

    public function setCursorPosition($cursorX, $cursorY)
    {
        $this->cursorX = $cursorX;
        $this->cursorY = $cursorY;
    }

    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;
    }

    public function getFontSize()
    {
        return $this->fontSize;
    }

    public function prepareCursorForCenterAlign()
    {
        $this->fontSize = 1;
        $textMaxWidth   = intval(0.8 * $this->width);

        do {

            $this->fontSize = $this->fontSize + 1;

            $p = imagettfbbox($this->fontSize, 0, $this->fontFile, $this->text);

            $textWidth  = $p[2] - $p[0];
            $textHeight = $p[1] - $p[7];
        } while ($textWidth <= $textMaxWidth);

        $this->cursorX = ($this->width - $textWidth) / 2;
        $this->cursorY = ($this->height - $textHeight) / 2;

    }

    public function getCursorPosition()
    {
        return array(
            'cursorX' => $this->cursorX,
            'cursorY' => $this->cursorY,
        );
    }

    public function cloneImageResource($image)
    {

        $width  = imagesx($image);
        $height = imagesy($image);
        $trans  = imagecolortransparent($image);

        if (imageistruecolor($image)) {
            $clone = imagecreatetruecolor($width, $height);
            imagealphablending($clone, true);
            imagesavealpha($clone, true);
        } else {
            $clone = imagecreate($width, $height);
            if ($trans >= 0) {
                $rgb = imagecolorsforindex($image, $trans);
                imagesavealpha($clone, true);
                $transIndex = imagecolorallocatealpha(
                    $clone, $rgb['red'], $rgb['green'], $rgb['blue'], $rgb['alpha']
                );
                imagefill($clone, 0, 0, $transIndex);
            }
        }

        imagecopy($clone, $image, 0, 0, 0, 0, $width, $height);
        return $clone;
    }

    public function save($outputFile)
    {
        $extension         = $this->getExtension($outputFile);
        $imageSaveFunction = 'image' . $extension;

        $image     = $this->cloneImageResource($this->backgroundImage);
        $fontColor = imagecolorallocate(
            $image, $this->colorR, $this->colorG, $this->colorB
        );
        imagettftext(
            $image, $this->fontSize, 0, $this->cursorX, $this->cursorY,
            $fontColor, $this->fontFile, $this->text
        );
        @$imageSaveFunction($image, $outputFile);

        imagedestroy($image);
    }

    public function show()
    {
        header('Content-Type: image/' . $this->extension);
        $imageSaveFunction = 'image' . $this->extension;

        $image     = $this->cloneImageResource($this->backgroundImage);
        $fontColor = imagecolorallocate(
            $image, $this->colorR, $this->colorG, $this->colorB
        );
        imagettftext(
            $image, $this->fontSize, 0, $this->cursorX, $this->cursorY,
            $fontColor, $this->fontFile, $this->text
        );
        @$imageSaveFunction($image);
        imagedestroy($image);
    }

}
