<!DOCTYPE html>
<html>

<head>
	<title>LOGO Generator</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<style>
		#logoGeneratorWrapper {
			margin: 10px;
			padding: 0px;
			width: 650px;
		}

		#logoGeneratorWrapper ul {
			width: 100%;
			margin: 0px;
			padding: 0px;
			list-style: none;
		}

		#logoGeneratorWrapper ul li {
			margin: 2px;
			list-style: none;
			width: 200px;
			height: 75px;
			overflow: hidden;
			display: inline-block;
		}

		#logoGeneratorWrapper ul li input[type=radio] {
			display: none;
		}

		#logoGeneratorWrapper ul li label {
			background-image: url(data:image/gif;base64,R0lGODlhBAGqAPcAAP///7Ozs/v7+9bW1uHh4fLy8rq6uoGBgTQ0NAEBARsbG8TExJeXl/39/VRUVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAAACwAAAAABAGqAAAI/wAbCBxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izat3KtavXr2DDih1LtqzZs2jTql3Ltq3bt3Djyp1Lt67du3jz6t3Lt6/fv4ADCx5MuLDhw4gTK17MuLHjx5AjS55MubLly5gza97MubPnz6BDix5NurTp06hTq17NurXr10QXHFgAO+qB2wUJFKgtdICBgrcPEBxAnDfQArcDEAw+sADxAcaBBocukLnA5wSi/zRwm8FA6wSea/8HyuD27wbWsY//OeBA+d23HTQIX3wg/ezrXw5gwID2wADmGRRec88NgF9+LS3AH3/UtedfQgXWh6BLCy54oEIRXjhhS/tV2NB9G9KkIAPUMaRhiDERcB6KVUVI3G4sljTAAgbUaCNELkoYo0g29rhiQ88toN6OI83o449EPnVikisteRCITDZJnJMFRQhjlCU5V6BDLmJJUpcKDSgQfVN6GRKZBg6UZm7i2VemmR9BeV2ZbT53JZwqadlgfWTiydKQbTYwpJ8n9TlniYYSatKggQqqo6JF6tjofFRC2lEBGk5qqUvhVbrpp6CGKuqopJZq6qmopqrqqqy26uqrsMb/KuustNZq66245vpQAbzeWZAABASrW0K98kqQAMUWW5CyyPYqQHO9LiussM/WRmONBwngm48LLOlbADXeaQC45Np44LXnjVsjojU+WAC6PXp62rZINlDAkfEu62OJDeBr7nXtCuQjjCoaQF3B+Mpr2rUPCiRAvM8SgO6d227bsI3UvWvjswwL3KN/BWencY3D2hssbxXnZuOd95LscMsGjHzlvw0g+2/K/Q4sqMv0+hpdx8MFXBDQDVwLHc45G3Dhvx0/THLA22YHNLDT8obx0OsahDPMBhJNs8dKF72uzTUKcK3ELv9Ldta12djwzgYsUO1AGHPsb9hJH4hwdm7bzntjwUKm7bK96BIwt2tXDwTs1/P9izC+1H2N7rN1t0yb0z3uxvDcjCPe7rQFYG4wr/T6N7maG3MdHrqRj93ywQMv/jmmjmddMHS3iwazjyLfLXfjg49p7u7cPgtzdoAPBC+M9B6JvMshA69wZcSv/Ky23Bpec+YFuV29wQceDzy71o8Jb+1hRx89bM7i1KyxulZ0ePz012///fjnr//+/Pfv//8ADKAAB0jAAhrwgAhMoAIXyMAGOvCBEIygBCdIwQpa8IIYzKAGN8hBXQUEACH5BAUKAAAALHIAOwAYABIAAAiAAAEIHEiwIIABBhMqZACAIUEDCxRGVDiQwYEDCRloLOhQ4IKLGA1q7JgQpIGECzYS7GgAJMWGDCYSJABSZsaGBARabOhSIAEDBhBWVFmwZU6BA4ACPUqAgdCXSJVCLAA14U+pVRUmPZlVYYGnXcN6HUC2LNiqZs2KJVB2QVmxAQEAIfkEBQoAAAAscwA7AB0ADgAACIkAAQgcSHDgAAYFEypcKBBhwgEMBxKASJCBRYIBIhI0YIBiQwAOAVAMyZCjAQIDLYYkqXGAyZQKWTJcwHHkxQIgRRbEqZCASZwIERr4ONDAgQNDBwxAOZBmAI9NCS44evSkUqU8CwyNeJDqUYNXCTDV6PUAVJFXzy40eiBjT6VjNTKIm1CsRo0BAQAh+QQFCgAAACx5ADsAGQARAAAIgQABCBwIgIABgggTKgSwwMAChA8XFiCA0IBFggwyLhwwoOLFgRo3ciww0OJBkAwiJiTAsaNAkwIXhFwIoCVFADAJMACwk6ZAmzg/zkTo8ifHmwQH9Ky5NOEApAqR7gzgk2bPplUhxszq0wBJrmDDJjxAtuwBlWDNmhULgKwDswsCAgAh+QQFCgAAACyAADsAEgAYAAAIggABLCAAoKDBgwgHLEAIYABBhgIXLDS4wIABiBEnAiBg8SJEiRo7DsAIsuCAjhgzAijQ8eFHiQ0DGNAokAGDlAYKHLTJgCZGmwCApgwa9ObQnUaPHnSptGnTpClvSnV6wCCDkU6bGmB61MCBAwGofv3qE+IABmMPQMWZ1qPSAGQBBAQAIfkEBQoAAAAsgAA7ABIAHgAACJkAAQwoAKCgwYMIBQ5IyLDggIcHByxY0PDhQoMTLya0aFAixYoQC078yJAjgJENHYbMGNGAAQIMCWgk4NKlxoYLahog2VDny5QdbQJFOHCoUaMMkioVCJQBAKdQnaZcmvSo1YMErX40cNUgg5sNCQQ4OJahgQMwBVZtyOCA24MGnIIFQMDtAZ5Az761apfr0QF2r9rNetQlw4AAIfkEBQoAAAAsgQA8ABEAHwAACIwAAQgcCIBAAYIIEw4YkLBhgYUNIy4kEJEggYUMKwq8CFHjxo4DOXoUiHEAxYocMWosmVEjx5MjCcAcSROhgZs4F2jEydNAxZ4GdNYcSnMAAwZCPR49WnPp0oY6Wwo8mlSgT6YDdTIQeKArgIMkI3Y9QBBrwrEDt1ZES5PtVLVnDzhQ2NCtxwUHqmoMCAAh+QQFCgAAACx6AEkAGAASAAAIfQABCBxIsCCAAQgTDihgsOFAhQodOky4ICEBiRgzYhygMaMBAxw7GvxI8qLIgQUWkDSwQOQABiYJrAx58ICBhgwMDvhoEgCDAwdyEiTAoCjGBUAPmBQKYAFTh0lvOizacmRSgk99GjWYtCrGrAKvYj1Q0GtZqQNzgj1Js2NAACH5BAUKAAAALHQATQAdAA4AAAiJAAEIHEiwoEEABA4SJMBAoUECAwYkPGjgwAEDDglG3FhAo8WPGTVujCiQwccDDAZkJIBx4EiWJ1smXGBwgAEDNAdCVFnxQACCPwFgZEAUQIGbBiY+LKgSQEOnUAHYvBly4FOCRQUuuNm0alSoV1lSDVm0K9GrALa2rIoWrUulDrtiDeqV7dqMAQEAIfkEBQoAAAAscgBKABkAEQAACIAAFxwYSPAAgIMIEyo8WLDgwocJBRJ0MBCixYsYMRYwkLHjgo4PGYCEGCBhAAIYB0BkwEDlwZYLCQyYmdAlR4QsRQIwwBOAzJkFEgZVyBIhz5s/XVpk+XFnz4MzB6B8uKCo0adQaS5NeLSm1oVWr95EOBViU4QLDJwdqdBA2YQBAQAh+QQFCgAAACxzAEMAEQAYAAAIdwAHHDhgAIDBgwgPGhg4cEHChwAYDmQwACLCBRIPWEwYYGDBjRxBihxJsqTJkyg3LmDAoCRLliBfvgS5siXCAh8fsnR4cIEBAxUHCDVIICGBnwYKABBaESLSpkwhDkB6MOpDpEUNWk34s6nWoQ8JeP06FqRYAAEBACH5BAUKAAAALHIAPQAOAB0AAAiKAAEIHAhggQGCCAceOMAgIcKFBwY4HBgA4kSFCw9eXGCRYEOCED96lAiAwIEABFGKJDBxwEeRExcIlHkRQAGNNVsy2MkTpsCePRO63AmAZ86jCEleHGDAgFKEBJpKdWhQ6oICQq0OJDAAq8CoTgkOGMtSoNeBBcYOKOtQLduEXMfWVJtTbs4CbwMCADs=);
			background-size: 100% 100%;
			display: block;
			height: 100%;
			border: 2px solid #F1F1F1;
			cursor: pointer;
		}

		#logoGeneratorWrapper ul li input[type=radio]:checked+label {
			border: 2px solid red;
		}

		#logoGeneratorWrapper ul li label img {
			width: 100%;
			height: 100%;
		}

		#logoGeneratorWrapper form input[type=color] {
			width: 30px;
			padding: 0px;
		}

		#logoGeneratorWrapper form input[type=text] {
			width: 350px;
			margin-right: 18px;
		}

		#logoGeneratorWrapper form button {
			width: 100px;
		}
	</style>
	<script type="text/javascript" src="js/angular.js"></script>
</head>

<body ng-app="app" ng-controller="logoGenerator">
	<div id="logoGeneratorWrapper">
		<ul>
			<li ng-repeat="logo in logos">
				<input name="logoFont" type="radio" id="{{logo.id}}" ng-checked="$index==selectedLogoIndex">
				<label for="{{logo.id}}" ng-click="changeSelectedLogoIndex($index);">
					<img alt="" ng-src="{{logo.url}}">
				</label>
			</li>
		</ul>
		<form class="form-inline">
			<div class="form-group">
				<input type="color" class="form-control" ng-model="logos[selectedLogoIndex].color">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" ng-model="logos[selectedLogoIndex].text">
			</div>
			<button id="logoUpdater" ng-click="updateLogo()" class="btn btn-default">
				Update
			</button>
			<button id="logoSaver" class="btn btn-success" ng-click="saveLogo()">
				Save
			</button>
		</form>
	</div>
</body>
<script type="text/javascript">
	var app = angular.module('app', []);
	app.controller('logoGenerator', function ($scope, $http) {
		var logoImageSaveUrl = 'image.php';
		var logoFonts = [
			'alexbrush', 'arizonia', 'capture', 'ffftusj', 'greatvibes',
			'josefinsans', 'kaushan', 'lobster', 'pacifico'
		];
		$scope.logos = [];
		$scope.selectedLogoIndex = 0;

		var capitalize = function (str) {
			return str.charAt(0).toUpperCase() + str.slice(1);
		};

		var getLogoGeneratorUrl = function (logo) {
			var baseUrl = 'image.php';
			var t = encodeURIComponent(logo.text);
			var f = encodeURIComponent(logo.font);
			var c = encodeURIComponent(logo.color);
			return baseUrl + '?t=' + t + '&c=' + c + '&f=' + f;
		};

		var createLogo = function (logoText, logoFont, logoColor) {
			var logo = {
				text: logoText, font: logoFont, color: logoColor,
				id: 'logoFont' + capitalize(logoFont)
			};
			logo.url = getLogoGeneratorUrl(logo);
			return logo;
		};

		$scope.initializeLogos = function () {
			var logoText = 'LOGO';
			var logoColor = '#DA6666';
			$scope.logos = [];
			for (var ii = 0; ii < logoFonts.length; ii++) {
				$scope.logos.push(createLogo(
					logoText, logoFonts[ii], logoColor
				));
			}
		};
		$scope.initializeLogos();

		$scope.changeSelectedLogoIndex = function (index) {
			$scope.selectedLogoIndex = index;
		};

		$scope.updateLogo = function () {
			var index = $scope.selectedLogoIndex;
			$scope.logos[index].url = getLogoGeneratorUrl(
				$scope.logos[index]
			);
		};

		$scope.saveLogo = function () {
			var index = $scope.selectedLogoIndex;
			$http.post(logoImageSaveUrl, {
				t: encodeURIComponent($scope.logos[index].text),
				f: encodeURIComponent($scope.logos[index].font),
				c: encodeURIComponent($scope.logos[index].color)
			});
		};
	});
</script>

</html>